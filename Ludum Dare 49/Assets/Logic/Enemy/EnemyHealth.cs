using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health = 100;
    public bool isBoss;

    void Start()
    {
        GameService.Event += OnGameEvent;
    }

    private void OnDestroy()
    {
        GameService.Event -= OnGameEvent;

        if (!isBoss) return;
        int currentStoryIndex = PlayerPrefs.GetInt("Story") + 1;

        if (currentStoryIndex > 6)
        {
            currentStoryIndex = 6;
        }
        
        PlayerPrefs.SetInt("Story", currentStoryIndex);
        PlayerPrefs.Save();
        
        GameObject.FindWithTag("BossDefeated").GetComponent<Canvas>().enabled = true;
        Time.timeScale = 0;
    }

    void Update()
    {
        if (health > 0) return;
        GameService.NotifyEvent(GameEventTypes.EnemyDied);
        Destroy(gameObject);
    }

    private void OnGameEvent(GameEventTypes eventType, GameObject triggerObject)
    {
        switch (eventType)
        {
            case GameEventTypes.DamageEnemy:
                if (gameObject.GetInstanceID() != triggerObject.GetInstanceID()) return;
                health -= GameService.hasDamagePowerUp ? 35 : 25;
                break;
        }
        
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float movementSpeed = 6.0f;
    public float rotationSpeed = 6.0f;
    public float pushForce;
    public bool isBoss;

    private Vector3 wayPointPos;
    private GameObject wayPoint;
    private Rigidbody2D enemyRigidbody;

    private void Awake()
    {
        enemyRigidbody = gameObject.GetComponent<Rigidbody2D>();
        wayPoint = GameObject.FindGameObjectWithTag("Player");
    }

    void FixedUpdate ()
    {
        RotateEnemy();
        MoveEnemy();
    }

    private void RotateEnemy()
    {
        Vector3 vectorToTarget = wayPoint.transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - 90;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.fixedDeltaTime * rotationSpeed);
    }

    private void MoveEnemy()
    {
        if (Vector3.Distance(wayPoint.transform.position, enemyRigidbody.transform.position) > 0.1f)
        {
            enemyRigidbody.AddRelativeForce( Vector2.up * (movementSpeed * Time.fixedDeltaTime * 100f));
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        enemyRigidbody.AddRelativeForce( -Vector2.up * pushForce, ForceMode2D.Impulse);
        GameService.NotifyEvent(isBoss ? GameEventTypes.DamagePlayerAsBoss : GameEventTypes.DamagePlayer);
        GameService.NotifyEvent(GameEventTypes.DamageEnemy, gameObject);
    }
}

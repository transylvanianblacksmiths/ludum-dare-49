using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerShooting : MonoBehaviour
{
    public Transform bulletSpawn;
    public GameObject bullet;
    public float fireRate;
    public AudioSource gunSoundSource;
    public AudioClip gunSound;
    
    private PlayerActions inputActions;
    private bool canShoot;
    private float shootTimer;
    void Awake()
    {
        inputActions = new PlayerActions();
        inputActions.Movement.Shoot.performed += _ => Shoot();
        shootTimer = fireRate;
    }

    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void Update()
    {
        shootTimer -= Time.deltaTime;

        if (shootTimer <= 0)
        {
            canShoot = true;
            shootTimer = fireRate;
        }
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }

    private void Shoot()
    {
        if (!canShoot || Time.timeScale == 0) return;
        canShoot = false;
        Instantiate(bullet, bulletSpawn.position, transform.rotation);
        gunSoundSource.PlayOneShot(gunSound, 0.15f);
    }
}

﻿public enum GameEventTypes
{
    DamagePlayer,
    DamageEnemy,
    EnemyDied,
    PlayerDied,
    ParanoiaOn,
    ParanoiaOff,
    DamagePlayerAsBoss,
    MaxHealth
}
